#include <iostream>

#include "argumentparser/argumentparser.h"

int main(int argc, char** argv)
{
    argumentparser::ArgumentParser args = argumentparser::ArgumentParser(
	"ArgumentParserTest",
	"A simple program to test the functionality of the Argument Parser");

    args.addArgumentRequired("-b", "Binary",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::BINARY);
    args.addArgument("-f", "Float",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::FLOAT);
    args.addArgument("-i", "Integer",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::INT);
    args.addArgument("-s", "String",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::STRING);

    try {
	if (!args.parseArguments(std::size_t(argc), argv) && !args.isSet("-h")) {
	    std::cerr << "Some required arguments were not given. Terminating!"
		      << std::endl;
	    return 1;
	}
    } catch (const std::exception& e) {
	std::cerr << "Parsing command line parameter failed with explanation:\n"
		  << e.what() << std::endl;
	std::cerr << "Terminating ..." << std::endl;

	return 1;
    }

    if (args.getValue<bool>("-h")) {
	std::cout << args.programHelp() << std::endl;
	return 0;
    }

    bool b = args.getValue<bool>("-b");
    printf("Binary value b = %s\n", (b) ? "True" : "False");
    float f = args.getValue<float>("-f");
    printf("Float value f = %f\n", f);
    int i = args.getValue<int>("-i");
    printf("Int value i = %i\n", i);
    std::string s = args.getValue<std::string>("-s");
    printf("String value s = %s\n", s.c_str());

    bool isSetX = args.isSet("X");
    std::cout << "X is set? " << isSetX << std::endl;

    return 0;
}

/*
 * Class that parses an array of c strings to get argument values for a given
 * set of arguments
 *
 * Copyright (C) 2016  Filip Krumpe <filip.krumpe@fmi.uni-stuttgart.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include <exception>
#include <string.h>
#include <unordered_map>
#include <vector>

namespace argumentparser {

class ArgumentParser {
public:
  enum class ARGUMENT_TYPES { BINARY, FLOAT, INT, STRING };

  struct Parser {
    bool isSet() const { return mSet; };

    virtual void parse(std::string &) = 0;

  protected:
    bool mSet = false;
  };

  struct Argument {

    Argument(std::string &aShort, std::string &aDescription, bool aRequired,
             ARGUMENT_TYPES &aType)
        : mShortName(aShort), mLongName(""), mDescription(aDescription),
          mValue(""), mRequired(aRequired), mType(aType){};

    Argument(std::string &aShort, std::string aLong, std::string &aDescription,
             bool aRequired, ARGUMENT_TYPES &aType)
        : mShortName(aShort), mLongName(aLong), mDescription(aDescription),
          mValue(""), mRequired(aRequired), mType(aType){};

    std::string getLongName() const { return mLongName; };
    std::string getShortName() const { return mShortName; };

    bool isRequired() const { return mRequired; };

    std::string toString() const;

  private:
    std::string mShortName, mLongName, mDescription, mValue;
    bool mRequired;
    ARGUMENT_TYPES mType;
  };

  std::vector<Parser *> mParsers;
  std::unordered_map<std::string, Parser *> mParserMap;

  std::vector<Argument> mArguments;
  std::unordered_map<std::string, std::vector<Argument>::const_iterator>
      mArgumentMap;

public:
  ArgumentParser(std::string aProgName, std::string aProgDesc);

  bool addArgument(std::string aShort, std::string aDescription,
                   ARGUMENT_TYPES aType);
  bool addArgument(std::string aShort, std::string aLong,
                   std::string aDescription, ARGUMENT_TYPES aType);

  bool addArgumentRequired(std::string aShort, std::string aDescription,
                      ARGUMENT_TYPES aType);
  bool addArgumentRequired(std::string aShort, std::string aLong,
                      std::string aDescription, ARGUMENT_TYPES aType);

  template <typename T> T getValue(std::string aName) const;

  bool isComplete() const;
  
  bool isSet(std::string aName) const;

  bool parseArguments(std::size_t aArgumentCount, char **aArgs);

  std::string programHelp() const;

private:
  std::string mProgName;
  std::string mProgDescription;
};
}

#endif // ARGUMENTPARSER_H

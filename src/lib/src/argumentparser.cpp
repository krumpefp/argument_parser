/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2016  Filip Krumpe <filip.krumpe@fmi.uni-stuttgart.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <argumentparser/argumentparser.h>

#include <limits>

namespace argumentparser {

std::string ArgumentParser::Argument::toString() const
{
    std::string result = "    ";

    result += getShortName();
    result += (getLongName() != "") ? ", " + getLongName() : "";
    result += (isRequired()) ? "    [REQUIRED]" : "";
    result += " :\n        " + mDescription;

    return result;
}

ArgumentParser::ArgumentParser(std::string aProgName, std::string aProgDesc)
    : mProgName(aProgName)
    , mProgDescription(aProgDesc)
{
    std::string s = "-h", l = "--help", d = "Program Help";
    ARGUMENT_TYPES t = ARGUMENT_TYPES::BINARY;
    addArgument(s, l, d, t);
}
struct BinaryParser : ArgumentParser::Parser {
    void parse(std::string& aValue)
    {
	mValue = true;

	mSet = true;
    };

    bool value() const { return mValue; };

private:
    bool mValue = false;
};

struct FloatParser : ArgumentParser::Parser {
    void parse(std::string& aValue)
    {
	try {
	    mValue = std::stof(aValue);
	} catch (const std::invalid_argument& e) {
	    throw e;
	} catch (const std::out_of_range& e) {
	    throw e;
	} catch (const std::exception& e) {
	    printf("Found unhandled exception %s\n", e.what());
	}

	mSet = true;
    }

    float value() const { return mValue; };

private:
    float mValue;
};

struct IntParser : ArgumentParser::Parser {
    void parse(std::string& aValue)
    {
	try {
	    mValue = std::stoi(aValue);
	} catch (const std::invalid_argument& e) {
	    throw e;
	} catch (const std::out_of_range& e) {
	    throw e;
	} catch (const std::exception& e) {
	    printf("Found unhandled exception %s\n", e.what());
	}

	mSet = true;
    }

    int value() const { return mValue; };

private:
    int mValue;
};

struct StringParser : ArgumentParser::Parser {
    void parse(std::string& aValue)
    {
	mValue = aValue;

	mSet = true;
    };

    std::string value() const { return mValue; };

private:
    std::string mValue;
};

bool addParser(std::vector<ArgumentParser::Parser*>& aParsers,
    ArgumentParser::ARGUMENT_TYPES aType)
{
    switch (aType) {
    case ArgumentParser::ARGUMENT_TYPES::BINARY:
	aParsers.emplace_back(new BinaryParser());
	break;
    case ArgumentParser::ARGUMENT_TYPES::FLOAT:
	aParsers.emplace_back(new FloatParser());
	break;
    case ArgumentParser::ARGUMENT_TYPES::INT:
	aParsers.emplace_back(new IntParser());
	break;
    case ArgumentParser::ARGUMENT_TYPES::STRING:
	aParsers.emplace_back(new StringParser());
	break;
    default:
	printf("Argument type not yet supported!");
	return false;
    }

    return true;
}
bool ArgumentParser::addArgument(std::string aShortName,
    std::string aDescription,
    ARGUMENT_TYPES aType)
{

    if (mArgumentMap.count(aShortName) > 0)
	return false;

    if (!addParser(mParsers, aType))
	return false;

    mArguments.emplace_back(aShortName, aDescription, false, aType);
    auto itA = --mArguments.end();
    mArgumentMap.emplace(itA->getShortName(), itA);

    mParserMap.emplace(itA->getShortName(), mParsers.back());

    return true;
}

bool ArgumentParser::addArgument(std::string aShortName, std::string aLongName,
    std::string aDescription,
    ARGUMENT_TYPES aType)
{

    if (mArgumentMap.count(aShortName) > 0 || mArgumentMap.count(aLongName) > 0)
	return false;

    if (!addParser(mParsers, aType))
	return false;

    mArguments.emplace_back(aShortName, aLongName, aDescription, false, aType);

    auto itA = --mArguments.end();
    mArgumentMap.emplace(itA->getShortName(), itA);
    mArgumentMap.emplace(itA->getLongName(), itA);

    mParserMap.emplace(itA->getShortName(), mParsers.back());
    mParserMap.emplace(itA->getLongName(), mParsers.back());

    return true;
}

bool ArgumentParser::addArgumentRequired(std::string aShortName,
    std::string aDescription,
    ARGUMENT_TYPES aType)
{

    if (mArgumentMap.count(aShortName) > 0)
	return false;

    if (!addParser(mParsers, aType))
	return false;

    mArguments.emplace_back(aShortName, aDescription, true, aType);
    auto itA = --mArguments.end();
    mArgumentMap.emplace(itA->getShortName(), itA);

    mParserMap.emplace(itA->getShortName(), mParsers.back());

    return true;
}

bool ArgumentParser::addArgumentRequired(std::string aShortName,
    std::string aLongName,
    std::string aDescription,
    ARGUMENT_TYPES aType)
{

    if (mArgumentMap.count(aShortName) > 0 || mArgumentMap.count(aLongName) > 0)
	return false;

    if (!addParser(mParsers, aType))
	return false;

    mArguments.emplace_back(aShortName, aLongName, aDescription, true, aType);

    auto itA = --mArguments.end();
    mArgumentMap.emplace(itA->getShortName(), itA);
    mArgumentMap.emplace(itA->getLongName(), itA);

    mParserMap.emplace(itA->getShortName(), mParsers.back());
    mParserMap.emplace(itA->getLongName(), mParsers.back());

    return true;
}

template <>
bool ArgumentParser::getValue<bool>(std::string aName) const
{
    auto it = mParserMap.find(aName);
    if (it == mParserMap.end()) {
	throw std::invalid_argument("No argument with short name " + aName + " found");
    }
    return (static_cast<BinaryParser*>(it->second))->value();
}

template <>
float ArgumentParser::getValue<float>(std::string aName) const
{
    auto it = mParserMap.find(aName);
    if (it == mParserMap.end()) {
	throw std::invalid_argument("No argument with short name " + aName + " found");
    }
    return (static_cast<FloatParser*>(it->second))->value();
}

template <>
int ArgumentParser::getValue<int>(std::string aName) const
{
    auto it = mParserMap.find(aName);
    if (it == mParserMap.end()) {
	throw std::invalid_argument("No argument with short name " + aName + " found");
    }
    return (static_cast<IntParser*>(it->second))->value();
}

template <>
std::string
ArgumentParser::getValue<std::string>(std::string aName) const
{
    auto it = mParserMap.find(aName);
    if (it == mParserMap.end()) {
	throw std::invalid_argument("No argument with short name " + aName + " found");
    }
    return (static_cast<StringParser*>(it->second))->value();
}

bool ArgumentParser::isComplete() const
{
    for (auto& it : mArguments) {
	if (it.isRequired()) {
	    if (!mParserMap.at(it.getShortName())->isSet())
		return false;
	}
    }

    return true;
}

bool ArgumentParser::isSet(std::string aName) const
{
    auto it = mParserMap.find(aName);
    if (it != mParserMap.end()) {
	return it->second->isSet();
    }

    return false;
}

bool ArgumentParser::parseArguments(std::size_t aArgumentCount, char** aArgs)
{
    // parse the arguments (skip first as it is the program name)
    std::string argumentName = "";
    std::string argumentValue = "";
    for (std::size_t i = 1; i < aArgumentCount; ++i) {
	std::string token = std::string(aArgs[i]);
	// check if the string is an argument name
	bool newArgument = mParserMap.count(token) > 0;

	if (!newArgument) {
	    // append token to the value string  with leading space if value string is
	    // not empty
	    argumentValue += (argumentValue == "") ? token : " " + token;
	    token = "";
	} else if (argumentName == "") {
	    argumentName = token;
	    if (argumentValue != "") {
		printf("Ignoring unknown console parameters: %s\n",
		    argumentValue.c_str());
	    }
	    argumentValue = "";
	    continue;
	}

	if (newArgument || i == aArgumentCount - 1) {
	    // found a new argument - parse the old one
	    auto parser = mParserMap.find(argumentName);
	    if (parser != mParserMap.end()) {
		try {
		    parser->second->parse(argumentValue);
		} catch (const std::exception& e) {
		    printf("Error parsing argument %s with value: %s\n",
			argumentName.c_str(), argumentValue.c_str());
		    throw std::invalid_argument("An argument could not be parsed: " + argumentName + " " + argumentValue);
		}
	    }

	    argumentName = token;
	    argumentValue = "";
	}
    }
    // parse the last argument if existing
    if (argumentName != "") {
	auto parser = mParserMap.find(argumentName);
	if (parser != mParserMap.end()) {
	    try {
		parser->second->parse(argumentValue);
	    } catch (const std::exception& e) {
		printf("Error parsing argument %s value: %s\n", argumentName.c_str(),
		    argumentValue.c_str());
		throw std::invalid_argument("An argument could not be parsed: " + argumentName + " " + argumentValue);
	    }
	}
    }

    return isComplete();
}

std::string ArgumentParser::programHelp() const
{
    std::string help = "\nHelp - " + mProgName + "\n    " + mProgDescription;

    // Argument aDescription
    help += "\n\nDESCRIPTION\n";
    help += "    List of arguments available for the program:\n";

    for (auto& arg : mArguments) {
	help += arg.toString();
	help += "\n";
    }

    help += "\n\n";

    return help;
}
}

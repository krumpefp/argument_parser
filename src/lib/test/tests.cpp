#include <assert.h>
#include <iomanip>
#include <iostream>

#include "argumentparser/argumentparser.h"

int main(int argc, char** argv)
{
    auto args = argumentparser::ArgumentParser("Test Parser",
	"Parses test parameters");

    args.addArgumentRequired("-b", "Binary",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::BINARY);
    args.addArgument("-f", "Float",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::FLOAT);
    args.addArgument("-i", "Integer",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::INT);
    args.addArgument("-s", "String",
	argumentparser::ArgumentParser::ARGUMENT_TYPES::STRING);

    if (!args.parseArguments((size_t)argc, argv)) {
	// parsing arguments failed
	return 1;
    }

    if (!args.isComplete()) {
	std::cout << "Missing argument set to be complete" << std::endl;
	return 1;
    }

    if (args.isSet("-i")) {
	if (!args.isSet("-b")) {
	    std::cout << "Missing -b to be set" << std::endl;
	    return 1;
	}
	if (!args.isSet("-f")) {
	    std::cout << "Missing -f to be set" << std::endl;
	    return 1;
	}
	if (!args.isSet("-i")) {
	    std::cout << "Missing -i to be set" << std::endl;
	    return 1;
	}
	if (!args.isSet("-s")) {
	    std::cout << "Missing -s to be set" << std::endl;
	    return 1;
	}

	if (args.getValue<bool>("-b") != true) {
	    std::cout << "Missing value of -b to be true" << std::endl;
	    return 1;
	}
	if (args.getValue<float>("-f") != 1.25) {
	    std::cout << "Missing value of -f to be 1.234. Value in fact was "
		      << std::setprecision(20) << args.getValue<float>("-f")
		      << std::endl;
	    return 1;
	}
	if (args.getValue<int>("-i") != 6789) {
	    std::cout << "Missing value of -i to be 6789" << std::endl;
	    return 1;
	}
	if (args.getValue<std::string>("-s") != "test") {
	    std::cout << "Missing value of -s to be 'test'" << std::endl;
	    return 1;
	}
    }

    return 0;
}

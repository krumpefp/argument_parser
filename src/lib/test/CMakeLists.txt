add_executable(tests
	tests.cpp)

target_link_libraries(tests
	argumentparser)

add_test(NAME ArgParserTest COMMAND tests -b -f 1.25 -i 6789 -s test)
add_test(NAME ArgParserIncomplete COMMAND tests -b)
add_test(NAME ArgParserFail COMMAND tests)
set_property(TEST ArgParserFail PROPERTY WILL_FAIL true)

